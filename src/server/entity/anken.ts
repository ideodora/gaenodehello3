import { Identity } from "../types";
import { v4 as uuid } from 'uuid';
import { StaffRepository } from "../repository";
import { Entity, StaffIdentity, Staff, Depertment, Sales } from ".";

export class AnkenIdentity implements Identity {
    public readonly surrogate: string;

    constructor(surrogate?: string) {
        if (!surrogate) {
            surrogate = uuid();
        }
        this.surrogate = surrogate;
    }
}

export class ConstructionType {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class OrderAcurracy {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class Perspective {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class Name {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class Duration {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class Client {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class PublicPrivate {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export class Anken implements Entity {

    public readonly identity: AnkenIdentity;
    public name: Name;
    public client: Client;
    public publicPrivate: PublicPrivate;
    public salesDepertment: Depertment;
    public contractConstructPeriod: Duration;
    public constructionType: ConstructionType;
    public orderAccuracy: OrderAcurracy;
    public perspective: Perspective;
    public readonly creatorIdentity: StaffIdentity;
    public salesInChargeIdentity: StaffIdentity;

    constructor(
        identity: AnkenIdentity,
        name: Name,
        client: Client,
        publicPrivate: PublicPrivate,
        salesDepertment: Depertment,
        contractConstructPeriod: Duration,
        constructionType: ConstructionType,
        orderAccuracy: OrderAcurracy,
        perspective: Perspective,
        creatorIdentity: StaffIdentity,
        salesInChargeIdentity: StaffIdentity
    ) {
        this.identity = identity;
        this.name = name;
        this.client = client;
        this.publicPrivate = publicPrivate;
        this.salesDepertment = salesDepertment;
        this.contractConstructPeriod = contractConstructPeriod;
        this.constructionType = constructionType;
        this.orderAccuracy = orderAccuracy;
        this.perspective = perspective;
        this.creatorIdentity = creatorIdentity;
        this.salesInChargeIdentity = salesInChargeIdentity;
    }

    getSalesInCharge():Promise<Sales> {
        return new StaffRepository().staffOfId(this.salesInChargeIdentity.surrogate);
    }

    getCreator(): Promise<Staff> {
        console.log(this.creatorIdentity.surrogate);
        return new StaffRepository().staffOfId(this.creatorIdentity.surrogate);
    }
}


import {
    Anken,
    AnkenIdentity,
    Name,
    Client,
    PublicPrivate,
    Duration,
    ConstructionType,
    OrderAcurracy,
    Perspective
} from "./anken";
    
import { Depertment, Staff, StaffFactory, DepertmentIdentity } from '.';

export class AnkenFactory {

    create(name: string, creator: Staff): Anken {
        const identity = new AnkenIdentity();

        const _name = new Name(name);
        const client = new Client('name');
        const publicPrivate = new PublicPrivate('name');
        const salesDepertment = new Depertment(new DepertmentIdentity(), 'name');

        const salesInCharge = new StaffFactory().createSales('営業マン');

        const contractConstructPeriod = new Duration('name');
        const constructionType = new ConstructionType('name');
        const orderAccuracy = new OrderAcurracy('name');
        const perspective = new Perspective('name');

        const instance = new Anken(
            identity,
            _name,
            client,
            publicPrivate,
            salesDepertment,
            contractConstructPeriod,
            constructionType,
            orderAccuracy,
            perspective,
            creator.identity,
            salesInCharge.identity
        );
        return instance;
    }
}
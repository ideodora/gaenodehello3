import { Entity } from ".";
import { Identity } from "../types";
import { v4 as uuid } from 'uuid';


export interface Entity {
    readonly identity: Identity;
}

export class StaffIdentity implements Identity {
    public readonly surrogate: string;

    constructor(surrogate?: string) {
        if (!surrogate) {
            surrogate = uuid();
        }
        this.surrogate = surrogate;
    }
}

export class DepertmentIdentity implements Identity {
    public readonly surrogate: string;

    constructor(surrogate?: string) {
        if (!surrogate) {
            surrogate = uuid();
        }
        this.surrogate = surrogate;
    }
}

export class Depertment {
    public name: string;
    public readonly identity: DepertmentIdentity;

    constructor(identity: DepertmentIdentity, name: string) {
        this.identity = identity;
        this.name = name;
    }
}

export class Staff implements Entity {
    name: string;
    public readonly identity: StaffIdentity;
    depertmentIdentity: DepertmentIdentity;

    constructor(identity: StaffIdentity, depertmentIdentity: DepertmentIdentity, name: string) {
        this.identity = identity;
        this.name = name;
        this.depertmentIdentity = depertmentIdentity;
    }

    behaviour(): void {
        console.log("woa");
    }
}

export class Sales extends Staff {

}

export class StaffFactory {

    createStaff(name: string, depertment_id: string): Staff {
        const depertmentIdentity = new DepertmentIdentity(depertment_id);
        const identity = new StaffIdentity();
        const staff = new Staff(identity, depertmentIdentity, name);
        return staff;
    }

    createSales(name: string): Sales {
        const identity = new StaffIdentity();
        const depertment = new Depertment(new DepertmentIdentity(), '営業部署1');
        const sales = new Sales(identity, depertment.identity, name);
        return sales;
    }
}
import { StaffIdentity, DepertmentIdentity, Depertment, Staff } from "./entity";
import { DepertmentRepository, StaffRepository } from "./repository";

export class CreateSystemUser {
    async create(name: string) {
        const staffIdentity = new StaffIdentity();
        const depertmentIdentity = new DepertmentIdentity();

        const depertment = new Depertment(depertmentIdentity, 'システム管理');

        const depertmentRepo = new DepertmentRepository();

        const data = await depertmentRepo.begin();
        const transaction = data[0];

        const result = await depertmentRepo.add(depertment, transaction);

        const staff = new Staff(staffIdentity, depertmentIdentity, name);
        const staffRepository = new StaffRepository();
        const result2 = await staffRepository.add(staff, transaction);

        return transaction.commit();
    }
}

interface RetreiveStaffOfIdDTO {
    staff: Staff;
    depertment: Depertment;
}

export class RetreiveStaffOfId {
    async retrieve(id: string): Promise<RetreiveStaffOfIdDTO> {
        const staff_repo = new StaffRepository();
        const depertment_repo = new DepertmentRepository();

        const staff = await staff_repo.staffOfId(id);
        const depertment = await depertment_repo.depertmentOfId(staff.depertmentIdentity.surrogate);

        const dto = {
            staff: staff,
            depertment: depertment
        }

        return dto;
    }
}
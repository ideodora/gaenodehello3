import { Router, Request, Response, NextFunction } from 'express';
import { Staff } from './entity';
import { AnkenFactory } from './entity/AnkenFactory';
import { AnkenRepository } from './repository/anken';
import { StaffRepository } from './repository';

export class AnkenController {
    protected title: string;
    
    constructor() {
        this.title = 'TODO';
    }

    /**
     * create hogehogehogeogoe
     */
    public create(): Router {
        const router = Router();
        this.index(router);
        this.add(router);
        this.show(router);
        // this.del(router);
        return router;
    }

    // private del(router:Router) {
    //     router.post('/delete/:id', (req: Request, res: Response, next: NextFunction) => {
    //         console.log('del router');
    //         console.log(req.params.id)

    //         const model = new Book();
            
    //         model
    //             .del(req.params.id)
    //             .then((result) => {
    //                 this.title = 'Home | TODO';
    //                 this.render(req, res, 'del');
    //                 next();
    //             })
    //             .catch((err) => {
    //                 console.log('err');
    //                 next(500);
    //             });
    //     })
    // }

    private show(router:Router) {
        router.get('/show/:id', async (req: Request, res: Response, next: NextFunction) => {
            try {
                this.title = 'Home | TODO';
                this.render(req, res, 'show', { entity: {} });
                next();
            } catch (error) {
                next(error);
            }
        });
    }

    private index(router:Router) {
        router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            try {
                this.title = 'Home | TODO';
                this.render(req, res, 'anken/index', { entities: [] });
                next();
            } catch (error) {
                console.log('error');
                next(error);
            }
        })
    }

    private add(router: Router) {
        router.get('/add', (req: Request, res: Response, next: NextFunction) => {
            console.log('add router');

            this.title = 'Home | TODO';
            this.render(req, res, 'anken/add');
            next();
        });

        router.post('/add', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const staff:Staff = await new StaffRepository().staffOfId('16f5d667-bbfe-4bb3-ab4e-f8b80119b244');

                console.log('staff', staff);

                const anken = new AnkenFactory().create(req.body.name, staff);

                
                console.log(await anken.getCreator());
                // console.log(await anken.getSalesInCharge());
                const result = await new AnkenRepository().add(anken);

                if (result) {
                    this.title = 'Home | TODO';
                    this.redirect(res, '/anken');
                    next();
                } else {
                    next("couldnt save");  
                }

            } catch (error) {
                next(error);
            }
        });
    }

    private render(req:Request, res:Response, view:string, options?:Object): void {
        res.locals.BASE_URL = '/';
        res.locals.title = this.title;
        res.render(view, options);
    }

    private redirect(res: Response, url: string): void {
        res.redirect(url);
    }
}
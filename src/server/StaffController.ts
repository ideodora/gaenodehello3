import { Router, Request, Response, NextFunction } from 'express';
import { StaffFactory } from './entity';
import { StaffRepository, DepertmentRepository } from './repository';
import { CreateSystemUser, RetreiveStaffOfId } from './appService';

export class StaffController {
    protected title: string;
    
    constructor() {
        this.title = 'TODO';
    }

    /**
     * create hogehogehogeogoe
     */
    public create(): Router {
        const router = Router();
        this.index(router);
        this.add(router);
        this.addSystem(router);
        this.show(router);
        // this.del(router);
        return router;
    }

    // private del(router:Router) {
    //     router.post('/delete/:id', (req: Request, res: Response, next: NextFunction) => {
    //         console.log('del router');
    //         console.log(req.params.id)

    //         const model = new Book();
            
    //         model
    //             .del(req.params.id)
    //             .then(() => {
    //                     this.title = 'Home | TODO';
    //                     this.render(res, 'del');
    //                     next();
    //                 })
    //             .catch(() => {
    //                     console.log('err');
    //                     next(500);
    //                 });
    //     })
    // }

    private show(router:Router) {
        router.get('/show/:id', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const entity = await new RetreiveStaffOfId().retrieve(req.params.id);

                console.log(Object.keys(entity.staff));
                // const repo = new StaffRepository();
                // const entity = await repo.staffOfId(req.params.id);
                this.title = 'Home | TODO';
                this.render(res, 'staff/show', { entity: entity });
                next();
            } catch (error) {
                next(error);
            }
        });
    }

    private index(router:Router) {
        router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const repo = new StaffRepository();
                const entities = await repo.pageStaffs(1, 20);
                this.title = 'Home | TODO';
                this.render(res, 'staff/index', { entities: entities });
                next();
            } catch (error) {
                console.log('error');
                next(error);
            }
        })
    }

    private add(router: Router) {
        router.get('/add', async (req: Request, res: Response, next: NextFunction) => {

            try {
                const repo = new DepertmentRepository();
                const entities = await repo.pageDepertments(1, 20);
                
                this.title = 'Home | TODO';
                this.render(res, 'staff/add', { depertments: entities });
                next();
            } catch (error) {
                next(error);
            }
        });

        router.post('/add', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const staff = new StaffFactory().createStaff(req.body.name, req.body.depertment);
                const result = await new StaffRepository().add(staff);
                
                if (result) {
                    this.redirect(res, '/staff');
                } else {
                    next("couldnt save");  
                }

            } catch (error) {
                next(error);
            }
        });
    }

    private addSystem(router: Router) {
        router.get('/addSystem', async (req: Request, res: Response, next: NextFunction) => {

            try {
                this.title = 'Home | TODO';
                this.render(res, 'staff/addSystem');
                next();
            } catch (error) {
                next(error);
            }
        });

        router.post('/addSystem', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const result = await new CreateSystemUser().create(req.body.name);
               
                if (result) {
                    this.redirect(res, '/staff');
                } else {
                    next("couldnt save");
                }

            } catch (error) {
                next(error);
            }
        });
    }

    private render(res:Response, view:string, options?:Object): void {
        res.locals.BASE_URL = '/';
        res.locals.title = this.title;
        res.render(view, options);
    }

    private redirect(res: Response, url: string): void {
        res.redirect(url);
    }
}
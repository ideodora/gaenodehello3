import * as Datastore from '@google-cloud/datastore';
import { Query, QueryCallback, QueryOptions, QueryResult } from '@google-cloud/datastore/query';
import { CommitResponse } from '@google-cloud/datastore/request';
import { DatastoreTransaction, BeginTransactionResponse } from '@google-cloud/datastore/transaction';
import { SerializerInterface } from '../types';

export abstract class DatastoreModel<T> {

    private ds: Datastore;
    protected kind: string;
    protected serializer: SerializerInterface<T>;

    constructor(kind: string) {
        this.ds = new Datastore({
            projectId: 'ideodora-test-node-001'
        });
        this.kind = kind;
    }

    // protected fromDatastore(obj: any): any {
    //     obj.id = obj[Datastore.KEY].id;
    //     return obj;
    // }

    // Lists all books in the Datastore sorted alphabetically by title.
    // The ``limit`` argument determines the maximum amount of results to
    // return per page. The ``token`` argument allows requesting additional
    // pages. The callback is invoked with ``(err, books, nextPageToken)``.
    // [START list]
    public _list(limit: number, token: string): Promise<T[]> {
        const q = this.ds.createQuery(this.kind)
            .limit(limit)
            .order('name')
            .start(token);

        return this.ds.runQuery(q).then((results) => {
            const entities = results[0];
            // const populated = entities.map<T>(this.fromDatastore);
            const populated = entities.map(this.serializer.deserialize);

            return new Promise<T[]>((resolve, reject) => {
                resolve(populated)
            });
        });
    }

    public _read(id: string): Promise<T> {
        // const key = this.ds.key([this.kind, parseInt(id, 10)]);
        const key = this.ds.key([this.kind, id]);

        console.log(key);

        return this.ds.get(key).then((results) => {
            console.log(results);
            const ent = results[0];
            // return Promise.resolve<T>(this.fromDatastore(ent));
            return Promise.resolve(this.serializer.deserialize(ent));
        });
    }

    public _update(id: string, data: T, transaction?: DatastoreTransaction): Promise<[CommitResponse]> {
        let key;
        if (id) {
            // key = this.ds.key([this.kind, parseInt(id, 10)]);
            key = this.ds.key([this.kind, id]);
        } else {
            key = this.ds.key([this.kind]);
        }

        const entity = {
            key: key,
            // data: this.toDatastore<T>(data, ['description'])
            data: this.serializer.serialize(data)
        }

        if (transaction) {
            transaction.save(entity);
            return Promise.resolve<[CommitResponse]>(null);
        }

        return this.ds.save(entity);
    }

    public _create(data: T): Promise<[CommitResponse]> {
        return this._update(null, data);
    }

    // protected toDatastore<T>(obj: T, nonIndexed: any[]): any[] {
    //     nonIndexed = nonIndexed || [];
    //     const results = [];
    //     Object.keys(obj).forEach((key) => {
    //         if (obj[key] === undefined) {
    //             return;
    //         }
    //         results.push({
    //             name: key,
    //             value: obj[key],
    //             excludeFromIndexes: nonIndexed.indexOf(key) !== -1
    //         });
    //     });
    //     return results;
    // }

    public del(id: string): Promise<[CommitResponse]> {
        const key = this.ds.key([this.kind, parseInt(id, 10)]);
        return this.ds.delete(key);
    }

    public _begin(): Promise<[DatastoreTransaction, BeginTransactionResponse]> {
        const transaction = this.ds.transaction();
        return transaction.run();
    }
}


export abstract class Model {

    private ds: Datastore;
    protected kind: string;

    constructor(kind: string) {
        this.ds = new Datastore({
            projectId: 'ideodora-test-node-001'
        });
        this.kind = kind;
    }

    protected fromDatastore<T>(obj: any): T {
        obj.id = obj[Datastore.KEY].id;
        return obj;
    }

    // Lists all books in the Datastore sorted alphabetically by title.
    // The ``limit`` argument determines the maximum amount of results to
    // return per page. The ``token`` argument allows requesting additional
    // pages. The callback is invoked with ``(err, books, nextPageToken)``.
    // [START list]
    public _list<T>(limit: number, token: string): Promise<T[]> {
        const q = this.ds.createQuery(this.kind)
            .limit(limit)
            .order('name')
            .start(token);

        return this.ds.runQuery(q).then((results) => {
            const entities = results[0];
            const populated = entities.map<T>(this.fromDatastore);

            return new Promise<T[]>((resolve, reject) => {
                resolve(populated)
            });
        });
    }

    public _read<T>(id: string): Promise<T> {
        // const key = this.ds.key([this.kind, parseInt(id, 10)]);
        const key = this.ds.key([this.kind, id]);

        console.log(key);

        return this.ds.get(key).then((results) => {
            console.log(results);
            const ent = results[0];
            return Promise.resolve<T>(this.fromDatastore<T>(ent));
        });
    }

    public _update<T>(id: string, data: T, transaction?: DatastoreTransaction): Promise<[CommitResponse]> {
        let key;
        if (id) {
            // key = this.ds.key([this.kind, parseInt(id, 10)]);
            key = this.ds.key([this.kind, id]);
        } else {
            key = this.ds.key([this.kind]);
        }

        const entity = {
            key: key,
            data: this.toDatastore<T>(data, ['description'])
        }

        if (transaction) {
            transaction.save(entity);
            return Promise.resolve<[CommitResponse]>(null);
        }

        return this.ds.save(entity);
    }

    public _create<T>(data: T): Promise<[CommitResponse]> {
        return this._update<T>(null, data);
    }

    protected toDatastore<T>(obj: T, nonIndexed: any[]): any[] {
        nonIndexed = nonIndexed || [];
        const results = [];
        Object.keys(obj).forEach((key) => {
            if (obj[key] === undefined) {
                return;
            }
            results.push({
                name: key,
                value: obj[key],
                excludeFromIndexes: nonIndexed.indexOf(key) !== -1
            });
        });
        return results;
    }

    public del(id: string): Promise<[CommitResponse]> {
        const key = this.ds.key([this.kind, parseInt(id, 10)]);
        return this.ds.delete(key);
    }

    public begin(): Promise<[DatastoreTransaction, BeginTransactionResponse]> {
        const transaction = this.ds.transaction();
        return transaction.run();
    }
}

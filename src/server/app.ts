// Copyright 2018, Google LLC.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

import * as express from 'express';
import * as path from 'path';
import { IndexController } from './indexController';
import { AnkenController } from './ankenController';
import * as bodyParser from 'body-parser';
import { StaffController } from './StaffController';
import { DepertmentController } from './DepertmentController';

const app = express();

// [START hello_world]
// Say hello!
// app.get('/', (req, res) => {
//   res.status(200).send('Hello, world!');
// });
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', new IndexController().create());
app.use('/anken', new AnkenController().create());
app.use('/staff', new StaffController().create());
app.use('/depertment', new DepertmentController().create());
// [END hello_world]

if (module === require.main) {
  // [START server]
  // Start the server
  const server = app.listen(process.env.PORT || 8080, () => {
    const addr = server.address();
    const bind = typeof addr === "string"
      ? "pipe " + addr
      : "port " + addr.port
    console.log(`App listening on port ${bind}`);
  });
  // [END server]
}

module.exports = app;

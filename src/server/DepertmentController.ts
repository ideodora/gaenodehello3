import { Router, Request, Response, NextFunction } from 'express';
import { Depertment, DepertmentIdentity } from './entity';
import { DepertmentRepository } from './repository';

export class DepertmentController {
    protected title: string;
    
    constructor() {
        this.title = 'TODO';
    }

    /**
     * create hogehogehogeogoe
     */
    public create(): Router {
        const router = Router();
        this.index(router);
        this.add(router);
        this.show(router);
        // this.del(router);
        return router;
    }

    // private del(router:Router) {
    //     router.post('/delete/:id', (req: Request, res: Response, next: NextFunction) => {
    //         console.log('del router');
    //         console.log(req.params.id)

    //         const model = new Book();
            
    //         model
    //             .del(req.params.id)
    //             .then(() => {
    //                     this.title = 'Home | TODO';
    //                     this.render(res, 'del');
    //                     next();
    //                 })
    //             .catch(() => {
    //                     console.log('err');
    //                     next(500);
    //                 });
    //     })
    // }

    private show(router:Router) {
        router.get('/show/:id', async (req: Request, res: Response, next: NextFunction) => {
            try {
                this.title = 'Home | TODO';
                this.render(res, 'show', { entity: {} });
                next();
            } catch (error) {
                next(error);
            }
        });
    }

    private index(router:Router) {
        router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            try {
                const repo = new DepertmentRepository();
                const entities = await repo.pageDepertments(1, 20);
                this.title = 'Home | TODO';
                this.render(res, 'depertment/index', { entities: entities });
                next();
            } catch (error) {
                console.log('error');
                next(error);
            }
        })
    }

    private add(router: Router) {
        router.get('/add', (req: Request, res: Response, next: NextFunction) => {
            console.log('add router');

            this.title = 'Home | TODO';
            this.render(res, 'depertment/add');
            next();
        });

        router.post('/add', async (req: Request, res: Response, next: NextFunction) => {
            try {
                // assign manual user
                const staffId = "16f5d667-bbfe-4bb3-ab4e-f8b80119b244";

                const depertment = new Depertment(new DepertmentIdentity(), req.body.name);
                const result = await new DepertmentRepository().add(depertment);
                
                if (result) {
                    this.redirect(res, '/depertment');
                } else {
                    next("couldnt save");  
                }

            } catch (error) {
                next(error);
            }
        });
    }

    private render(res:Response, view:string, options?:Object): void {
        res.locals.BASE_URL = '/';
        res.locals.title = this.title;
        res.render(view, options);
    }

    private redirect(res: Response, url: string): void {
        res.redirect(url);
    }
}
import { AnkenIdentity, Anken } from "../entity/anken";
import { Entity, Staff, Depertment } from "../entity";

interface Identity {
    readonly surrogate: string;
}

interface SerializerInterface<T> {
    serialize(data: T): object;
    deserialize(data: object): T;
}

import { Router, Request, Response, NextFunction } from 'express';

export class IndexController {
    protected title: string;
    
    constructor() {
        this.title = 'TODO';
    }

    /**
     * create hogehogehogeogoe
     */
    public create(): Router {
        const router = Router();
        this.index(router);
        return router;
    }

    private index(router:Router) {
        router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            try {
                this.title = 'Home | TODO';
                this.render(res, 'index', { entities: [] });
                next();
            } catch (error) {
                console.log('error');
                next(error);
            }
        })
    }

    private render(res:Response, view:string, options?:Object): void {
        res.locals.BASE_URL = '/';
        res.locals.title = this.title;
        res.render(view, options);
    }
}
import { SerializerInterface } from "../types";
import * as Datastore from '@google-cloud/datastore';
import { DatastoreModel } from '../persistent';

import {
    Anken,
    AnkenIdentity,
    Name,
    Client,
    PublicPrivate,
    Duration,
    ConstructionType,
    OrderAcurracy,
    Perspective,
} from "../entity/anken";

import {
    Depertment,
    Staff,
    StaffFactory,
    DepertmentIdentity,
    StaffIdentity
} from "../entity";


interface AnkenRepositoryInterface {
    add(anken: Anken): Promise<boolean>;
}

class AnkenSerializer implements SerializerInterface<Anken> {
    serialize(data: Anken): object {
        const nonIndexed = [];
        const results = [];
        console.log(data);
        results.push({
            name: 'id',
            value: data.identity.surrogate,
            excludeFromIndexes: nonIndexed.indexOf('id') !== -1
        });
        results.push({
            name: 'name',
            value: data.name,
            excludeFromIndexes: nonIndexed.indexOf('name') !== -1
        });
        results.push({
            name: 'depertment',
            value: data.salesDepertment.name,
            excludeFromIndexes: nonIndexed.indexOf('depertment') !== -1
        });
        return results;
    }

    deserialize(data: object): Anken {

        const identity = new AnkenIdentity(data[Datastore.KEY].name);
        const _name = new Name(data['name']);
        const client = new Client('name');
        const publicPrivate = new PublicPrivate('name');
        const salesDepertment = new Depertment(new DepertmentIdentity(), 'name');

        const salesInCharge = new StaffFactory().createSales('営業マン');

        const contractConstructPeriod = new Duration('name');
        const constructionType = new ConstructionType('name');
        const orderAccuracy = new OrderAcurracy('name');
        const perspective = new Perspective('name');

        const creatorIdentity = new StaffIdentity();

        const instance = new Anken(
            identity,
            _name,
            client,
            publicPrivate,
            salesDepertment,
            contractConstructPeriod,
            constructionType,
            orderAccuracy,
            perspective,
            creatorIdentity,
            salesInCharge.identity
        );
        
        return instance;
    }
}

export class AnkenRepository extends DatastoreModel<Anken> implements AnkenRepositoryInterface {

    protected serializer: AnkenSerializer;

    constructor() {
        super('Anken');
        this.serializer = new AnkenSerializer();
    }

    // protected fromDatastore<Anken>(obj: any): Anken {
    //     obj.id = obj[Datastore.KEY].name;
    //     return obj;
    // }

    // protected toDatastore(anken: Anken, nonIndexed: any[]): any[] {
    //     nonIndexed = nonIndexed || [];
    //     const results = [];
    //     console.log(anken);
    //     results.push({
    //         name: 'id',
    //         value: anken.identity.surrogate,
    //         excludeFromIndexes: nonIndexed.indexOf('id') !== -1
    //     });
    //     results.push({
    //         name: 'name',
    //         value: anken.name,
    //         excludeFromIndexes: nonIndexed.indexOf('name') !== -1
    //     });
    //     results.push({
    //         name: 'depertment',
    //         value: anken.depertment.name,
    //         excludeFromIndexes: nonIndexed.indexOf('depertment') !== -1
    //     });
    //     return results;
    // }

    add(anken: Anken): Promise<boolean> {
        console.log('create');
        return this._update(anken.identity.surrogate, anken).then((results) => {
            console.log('create promisy');
            console.log(results[0].mutationResults[0].key);
            return Promise.resolve<boolean>(true);
        });
    }
}
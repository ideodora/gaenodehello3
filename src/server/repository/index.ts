import { SerializerInterface } from "../types";
import { DatastoreTransaction, BeginTransactionResponse } from "@google-cloud/datastore/transaction";
import * as Datastore from '@google-cloud/datastore';
import { DatastoreModel } from '../persistent';

import {
    Depertment,
    Staff,
    DepertmentIdentity,
    StaffIdentity
} from "../entity";


interface StaffRepositoryInterface {
    staffOfId(id: string): Promise<Staff>;
    pageStaffs(page: number, size: number): Promise<Staff[]>;
    add(staff: Staff): Promise<boolean>;
}

interface DepertmentRepositoryInterface {
    depertmentOfId(id: string): Promise<Depertment>;
    pageDepertments(page: number, size: number): Promise<Depertment[]>;
    add(depertment: Depertment): Promise<boolean>;
}

class StaffSerializer implements SerializerInterface<Staff> {
    serialize(data: Staff): any {
        const nonIndexed = [];
        const results = [];
        console.log(data);
        results.push({
            name: 'id',
            value: data.identity.surrogate,
            excludeFromIndexes: nonIndexed.indexOf('id') !== -1
        });
        results.push({
            name: 'name',
            value: data.name,
            excludeFromIndexes: nonIndexed.indexOf('name') !== -1
        });
        results.push({
            name: 'depertmentIdentity',
            value: data.depertmentIdentity.surrogate,
            excludeFromIndexes: nonIndexed.indexOf('depertmentIdentity') !== -1
        });
        return results;
    }

    deserialize(data: any): Staff {
        console.log('staff from data store');
        console.log(data);
        data.id = data[Datastore.KEY].name;
        const staff = new Staff(new StaffIdentity(data.id), new DepertmentIdentity(data.depertmentIdentity), data.name);
        return staff;
    }
}

export class StaffRepository extends DatastoreModel<Staff> implements StaffRepositoryInterface {

    protected serializer: StaffSerializer;

    constructor() {
        super('Staff');

        this.serializer = new StaffSerializer();
    }

    // protected fromDatastore<T>(obj: any): Staff {
    //     console.log('staff from data store');
    //     console.log(obj);
    //     // {
    //     //     id: '16f5d667-bbfe-4bb3-ab4e-f8b80119b244',
    //     //         [watch]   depertment: '部署１',
    //     //             [watch]   name: 'システムユーザー',
    //     //                 [watch][Symbol(KEY)]:
    //     //     [watch]    Key {
    //     //         [watch]      namespace: undefined,
    //     //             [watch]      name: '16f5d667-bbfe-4bb3-ab4e-f8b80119b244',
    //     //                 [watch]      kind: 'Staff',
    //     //                     [watch]      path: [Getter]
    //     //     }
    //     // }
    //     obj.id = obj[Datastore.KEY].name;
    //     const staff = new Staff(new StaffIdentity(obj.id), new DepertmentIdentity(obj.depertmentIdentity), obj.name);
    //     return staff;
    // }

    // protected toDatastore(staff: any, nonIndexed: any[]): any[] {
    //     nonIndexed = nonIndexed || [];
    //     const results = [];
    //     console.log(staff);
    //     results.push({
    //         name: 'id',
    //         value: staff.identity.surrogate,
    //         excludeFromIndexes: nonIndexed.indexOf('id') !== -1
    //     });
    //     results.push({
    //         name: 'name',
    //         value: staff.name,
    //         excludeFromIndexes: nonIndexed.indexOf('name') !== -1
    //     });
    //     results.push({
    //         name: 'depertmentIdentity',
    //         value: staff.depertmentIdentity.surrogate,
    //         excludeFromIndexes: nonIndexed.indexOf('depertmentIdentity') !== -1
    //     });
    //     return results;
    // }

    staffOfId(id: string): Promise<Staff> {
        const identity = new StaffIdentity(id);
        return this._read(identity.surrogate);
    }

    add(staff: Staff, transaction?: DatastoreTransaction): Promise<boolean> {
        return this._update(staff.identity.surrogate, staff, transaction).then((results) => {
            return Promise.resolve<boolean>(true);
        });
    }

    pageStaffs(page: number = 1, size: number = 10): Promise<Staff[]> {
        const limit = page * size;
        return this._list(limit, "");
    }
}

class DepertmentSerializer implements SerializerInterface<Depertment> {
    serialize(data: Depertment): any {
        const nonIndexed = [];
        const results = [];
        results.push({
            name: 'id',
            value: data.identity.surrogate,
            excludeFromIndexes: nonIndexed.indexOf('id') !== -1
        });
        results.push({
            name: 'name',
            value: data.name,
            excludeFromIndexes: nonIndexed.indexOf('name') !== -1
        });
        return results;
    }

    deserialize(data: any): Depertment {
        console.log('depertment from datastore');
        console.log(data);
        data.id = data[Datastore.KEY].name;
        return data;
    }
}

export class DepertmentRepository extends DatastoreModel<Depertment> implements DepertmentRepositoryInterface {

    protected serializer: DepertmentSerializer;

    constructor() {
        super('Depertment');

        this.serializer = new DepertmentSerializer();
    }

    // protected fromDatastore<Depertment>(obj: any): Depertment {
    //     console.log('depertment from datastore');
    //     console.log(obj);
    //     obj.id = obj[Datastore.KEY].name;
    //     return obj;
    // }

    // protected toDatastore(depertment: any, nonIndexed: any[]): any[] {
    //     nonIndexed = nonIndexed || [];
    //     const results = [];
    //     results.push({
    //         name: 'id',
    //         value: depertment.identity.surrogate,
    //         excludeFromIndexes: nonIndexed.indexOf('id') !== -1
    //     });
    //     results.push({
    //         name: 'name',
    //         value: depertment.name,
    //         excludeFromIndexes: nonIndexed.indexOf('name') !== -1
    //     });
    //     return results;
    // }

    depertmentOfId(id: string): Promise<Depertment> {
        const identity = new DepertmentIdentity(id);
        return this._read(identity.surrogate);
    }

    add(depertment: Depertment, transaction?: DatastoreTransaction): Promise<boolean> {
        return this._update(depertment.identity.surrogate, depertment, transaction).then(() => {
            return Promise.resolve<boolean>(true);
        });
    }

    pageDepertments(page: number = 1, size: number = 10): Promise<Depertment[]> {
        const limit = page * size;
        return this._list(limit, "");
    }

    begin(): Promise<[DatastoreTransaction, BeginTransactionResponse]> {
        return this._begin();
    }
}